# rsstube

`rsstube` accepts a YouTube link or a username and prints the RSS feed for that user's channel.

## Dependencies

Core utils like `curl` and `grep` are required.

Note: This script was written and tested with Bash. I tried to make it portable, but it may not work with all shells. Please feel free to let me know if it doesn't work with your shell.

## Installing

This script can be installed by running the `install` script as root, or by copying `rsstube` to `/usr/bin/`

To add the manual for `rsstube` (if manually installing), also copy `rsstube.1` to `/usr/local/share/man/man1/`, then run `mandb` as root.

The `install` script does not remove these files after copying them, but it is safe to do so if you wish.

## Usage

`rsstube [OPTIONS] username`

`rsstube [OPTIONS] <link to channel>`

`rsstube [OPTIONS] <link to video>`

## Options

`-q` - Make the `curl` call in silent mode.

`-p` - Specify a proxy argument for curl.

## Examples

`rsstube https://www.youtube.com/channel/UCqC_GY2ZiENFz2pwL0cSfAw`

`rsstube https://www.youtube.com/watch?v=XzIXc5CTC2M`

`rsstube -q https://www.youtube.com/watch?v=XzIXc5CTC2M`

`rsstube -p "socks5://localhost:9150" https://www.youtube.com/watch?v=XzIXc5CTC2M`

## Config Options

A config file can be specified in ~/.config/rsstube/config. In this case, the following options can currently be specified:

`proxy="<curl proxy option>"`

`quiet=--silent`

## How It Works

YouTube has a built-in RSS feed for each channel, located at `https://www.youtube.com/feeds/videos.xml?channel_id=[channel_id]`. If the parameter is a channel URL (i.e., `https://www.youtube.com/channel/[channel_id]`), then `rsstube` simply gets that last bit, the `channel_id`, and puts it into that link template.

If the parameter is a YouTube link that's not a channel URL, `rsstube` downloads the page with `curl` and grabs the `channel_id` from the metadata tags or the canonical link. It then does the above step.

If you don't want to use `rsstube`, you can follow that process manually to get the feed URL.
